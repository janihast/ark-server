# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'libvirt'

Vagrant.configure("2") do |config|


  config.vm.provider :libvirt do |v|
    v.memory = 8192
    v.cpus = 8
    v.storage :file, :size => '40G', :device => 'vdb', :path => 'ark-data.qcow2', :type => 'qcow2', :allow_existing => true

  end

  config.vm.define :ark do |ark|
    ark.vm.box = "peru/ubuntu-18.04-server-amd64"
    ark.vm.hostname = "ark"
    ark.vm.network :private_network, ip: "10.0.0.2"
    ark.vm.provision :shell, privileged: true, inline: $provision
    ark.vm.network :public_network,
      :dev => "br0",
      :mode => "bridge",
      :type => "bridge",
      :mac => "52:54:00:6e:e7:84"
  end

end

$provision = <<-PROVISION
set -e
export DEBIAN_FRONTEND=noninteractive
apt-get -qq update
apt install -qq -y debconf-utils vim
echo "steamcmd	steam/question	select	I AGREE" | debconf-set-selections
echo "steamcmd  steam/license note" | debconf-set-selections
apt install -qq -y lib32gcc1 steamcmd 

if [ ! -b /dev/vdb1 ]; then
  echo "Creating disk for Ark"
  sudo parted -s -a optimal /dev/vdb mktable gpt unit mib mkpart primary 0% 100%
  mkfs.ext4 /dev/vdb1
fi

echo "Mounting disk for Ark"
cat <<EOF >>/etc/fstab
/dev/vdb1        /ark-data         ext4    defaults,noatime,discard        0 1
EOF

mkdir -p /ark-data
mount /ark-data
chown vagrant:vagrant /ark-data

cat <<EOF >/etc/systemd/system/ark.service
[Unit]
Description=ARK: Survival Evolved dedicated server
Wants=network-online.target
After=syslog.target network.target nss-lookup.target network-online.target

[Service]
ExecStartPre=/usr/games/steamcmd +login anonymous +force_install_dir /ark-data +app_update 376030 validate +quit
ExecStart=/ark-data/ShooterGame/Binaries/Linux/ShooterGameServer Valguero_P?listen -server -log
WorkingDirectory=/ark-data
LimitNOFILE=100000
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s INT $MAINPID
User=vagrant
Group=vagrant
TimeoutStartSec=1800

[Install]
WantedBy=multi-user.target
EOF

echo "Starting Ark server"
systemctl daemon-reload
systemctl enable ark
systemctl start ark
PROVISION
